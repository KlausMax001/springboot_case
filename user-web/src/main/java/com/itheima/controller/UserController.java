package com.itheima.controller;

import com.itheima.pojo.User;
import com.itheima.service.UserService;
import org.apache.dubbo.config.annotation.DubboReference;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author Guo Lixin
 * @date 2021/3/9 17:52
 */
@RestController
@RequestMapping("/user")
public class UserController {
    @DubboReference(check = false)
    private UserService userService;

    @RequestMapping("/findAll")
    public List<User> findAll() {
        return userService.list();
    }

}
