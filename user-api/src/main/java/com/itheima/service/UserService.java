package com.itheima.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.itheima.pojo.User;

/**
 * @Author Guo Lixin
 * @date 2021/3/9 17:36
 */
public interface UserService extends IService<User> {
}
