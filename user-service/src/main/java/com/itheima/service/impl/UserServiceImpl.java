package com.itheima.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.itheima.mapper.UserMapper;
import com.itheima.pojo.User;
import com.itheima.service.UserService;
import org.apache.dubbo.config.annotation.DubboService;

/**
 * @Author Guo Lixin
 * @date 2021/3/9 17:44
 */
@DubboService(timeout = 500000)
public class UserServiceImpl extends ServiceImpl<UserMapper, User>  implements UserService {
}
