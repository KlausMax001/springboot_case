package com.itheima.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.itheima.pojo.User;
import org.springframework.stereotype.Repository;

/**
 * @Author Guo Lixin
 * @date 2021/3/9 17:47
 */
@Repository
public interface UserMapper extends BaseMapper<User> {
}
